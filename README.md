Some helpful minimal Rust crates.

### async-executor

A very basic queue-based async executor.

### debug-impl

A wrapper for non-debug structs, that implements `fmt::Debug`.

### dyn-error

Dynamic errors, intended for passing errors from dynamically loaded libraries.

### serde-dyn-repr

A dynamic representation for deserialized data.

### stdout-log

A simple logger that logs to `std::out`.